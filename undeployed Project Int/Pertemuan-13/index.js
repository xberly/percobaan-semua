// var person = {
//     Name : "lip",
//     Age : 21,
//     Gender :"male",
//     Interest : "Coding",
//     sentence : function(){
//     return "Hi I'm " + person.Interest;
//     }
//    };

//    person.sentence();
// console.log(person.sentence());

// var person = {
//     Name : "liplip",
//     Age : 21,
//     Gender :"male",
//     Interest : "Coding",
//     Greeting : function(){
//     return "Hi I'm " + person.Name;
//     },
//     introduction:function(){
//     return "Hi My Name is " +this.Name+ " I'm "+this.Age;
//     }
//    };

//    person.Greeting();
// console.log(person.Greeting());
// person.introduction();
// console.log(person.introduction());

var canvas = document.getElementById("myCanvas");
 var mystyle = canvas.getContext("2d");
 // Wellcome Text
 mystyle.fillStyle = "red";
 
 mystyle.textAlign = "center";
 mystyle.font = "30px cursive";
 mystyle.fillText("Welcome", 325, 60);
 mystyle.fillText("This is my canvas", 325, 90);
 // rectangle
 mystyle.fillStyle="red";
 mystyle.fillRect(200, 325, 250, 50);
 // coba clearRect
 mystyle.clearRect(215,338,220,25);
 // make a triangle
 mystyle.fillStyle="red";
 mystyle.beginPath();
 mystyle.moveTo(325, 230);
 mystyle.lineTo(300, 270);
 mystyle.lineTo(350, 270);
 mystyle.closePath();
 mystyle.fill();
 // make a circle
 mystyle.fillStyle = "black";
 mystyle.strokeStyle = "white";
 mystyle.lineWidth = "20";
 mystyle.beginPath();
 mystyle.arc(130, 150, 40, 0, 2 * Math.PI);
 mystyle.fill();
 mystyle.stroke();
 
 // make a circle 2
 mystyle.fillStyle = "black";
 mystyle.strokeStyle = "white";
 mystyle.lineWidth = "20";
 mystyle.beginPath();
 mystyle.arc(524, 150, 40, 0, 2 * Math.PI);
 mystyle.fill();
 mystyle.stroke();